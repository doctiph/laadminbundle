<?php

namespace La\AdminBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\AdminBundle\Event\AdminNavEvent;
use La\AdminBundle\Controller\AdminController;

class AdminListener implements EventSubscriberInterface
{
    const ADMIN_NAV_EVENT_PRIORITY = 1;

    protected $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'admin.nav.links' => array('onAdminNavLinksRequest', static::ADMIN_NAV_EVENT_PRIORITY)
        );
    }

    public function onAdminNavLinksRequest(AdminNavEvent $event)
    {

        $event->addLinks(
            array(
                'name' => 'la_admin.nav.home.default',
                'icon' => 'home',
                'links' => array(
                    array(
                        'name' => 'la_admin.nav.home.welcome',
                        'url' => $this->router->generate('la_admin_homepage'),
                        'role' => 'ROLE_LA_ADMIN',
                    )
                )
            )
        );


        $event->addLinks(
            array(
                'name' => 'la_admin.nav.settings.users.default',
                'icon' => 'cog',
                'links' => array(
                    array(
                        'name' => 'la_admin.nav.settings.users.edit',
                        'url' => $this->router->generate('la_admin_settings_users'),
                        'role' => 'ROLE_LA_ADMIN_USER',
                    ),
                    array(
                        'name' => 'la_admin.nav.settings.users.create',
                        'url' => $this->router->generate('la_admin_settings_user_create'),
                        'role' => 'ROLE_LA_ADMIN_USER',
                    )
                )
            )
        );
        $event->addLinks(
            array(
                'name' => 'la_admin.nav.account.default',
                'icon' => 'lock',
                'links' => array(
                    array(
                        'name' => 'la_admin.nav.account.edit',
                        'url' => $this->router->generate('la_admin_account'),
                        'role' => 'ROLE_LA_ADMIN_USER_PROFILE',
                    )
                )
            )
        );

         //
    }

}