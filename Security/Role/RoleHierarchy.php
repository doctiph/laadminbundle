<?php

namespace La\AdminBundle\Security\Role;

use  Symfony\Component\Security\Core\Role\RoleHierarchy as CoreRoleHierarchy;

class RoleHierarchy extends CoreRoleHierarchy
{

    /**
     * @param array $hierarchy
     * @param $roles
     */
    public function __construct(array $hierarchy, $roles)
    {
        $roles = $this->buildRolesTree($roles);
        parent::__construct(array_merge($roles, $hierarchy));
    }

    /**
     * @param $roles
     * @return array
     */
    private function buildRolesTree($roles)
    {
        $hierarchy = array();
        foreach ($roles as $roleName => $role) {
            if ($role['parent']) {
                if (!isset($hierarchy[$role['parent']])) {
                    $hierarchy[$role['parent']] = array();
                }
                $hierarchy[$role['parent']][] = $roleName;
            } else {
                if (!isset($hierarchy[$roleName])) {
                    $hierarchy[$roleName] = array();
                }
            }
        }
        return $hierarchy;
    }


}