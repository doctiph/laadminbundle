<?php
namespace La\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller managing the user profile
 *
 */
abstract class AdminController extends Controller
{

    protected function history($key, $value)
    {
        if (empty($value)) {
            return;
        }

        if (!$this->get('session')->has('admin_history')) {
            $search = [];
            $this->get('session')->set('admin_history', $search);
        } else {
            $search = $this->get('session')->get('admin_history');
        }

        if (isset($search[$key])) {
            $searchKey = $search[$key];
        } else {
            $searchKey = [];
        }

        if (!in_array($value, $searchKey)) {
            $searchKey[] = $value;
        }
        $search[$key] = $searchKey;

        $this->get('session')->set('admin_history', $search);
    }

    protected function alert($type, $message, $clear = true)
    {
        $session = $this->container->get('session');
        if ($clear) {
            $session->getFlashBag()->clear();
        }
        $session->getFlashBag()->add($type, $message);
    }

}
