<?php

namespace La\AdminBundle\Controller;

use La\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\HttpFoundation\Request;
use La\AdminBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AccountController extends AdminController
{

    protected $currentMenu = 'la_admin.nav.account.default';

    /**
     * Me
     *
     **/
    public function profileAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_USER_PROFILE')) {
            throw new AccessDeniedException();
        }

        if ($request->query->has('admin_user')) {
            $username = $request->query->get('admin_user');
        }

        $user = $this->get('security.context')->getToken()->getUser();

        $oldPassword = $user->getPassword();
        $form = $this->container->get('form.factory')->createBuilder('admin_account', $user)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $userRepository = $em->getRepository('LaAdminBundle:User');
            $existingUser = $userRepository->findOneByUsername($user->getUsername());
            $existingEMail = $userRepository->findOneByEmail($user->getEmail());
            if (!is_null($existingUser) && ($user->getId() !== $existingUser->getId())) {
                $this->alert('danger', 'Le user est déjà dans la base');
            } elseif (!is_null($existingEMail) && ($existingEMail->getId() !== $user->getId())) {
                $this->alert('danger', 'Cet email est déjà dans la base');
            } else {
                if (null !== $user->getPassword()) {
                    $factory = $this->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    if ($user->getSalt() === '') {
                        $salt = md5(uniqid(null, true));
                        $user->setSalt($salt);
                    }
                    $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                    $user->setPassword($password);
                } else {
                    $user->setPassword($oldPassword);
                }

                $em->persist($user);
                $em->flush();

                $this->alert('success', 'Le user a été mis à jour');
            }
        }

        return $this->render('LaAdminBundle:Account:user.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_admin.nav.account.default'
        ));

    }


}