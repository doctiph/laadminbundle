<?php

namespace La\AdminBundle\Controller;

use La\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\HttpFoundation\Request;
use La\AdminBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UsersController extends AdminController
{

    protected $currentMenu = 'la_admin.nav.settings.users.default';

    /**
     * search a user
     *
     **/
    public function usersAction(Request $request)
    {

        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_USER')) {
            throw new AccessDeniedException();
        }

        $username = null;

        $form = $this->createFormBuilder()
            ->add('user', 'text', array('label' => 'Username'))
            ->add('search', 'submit', array('label' => 'Cherher'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $data = $form->getData();
            if (isset($data['user'])) {
                $username = $data['user'];
            }
        } else if ($request->query->has('admin_user')) {
            $username = $request->query->get('admin_user');
        }

        if (!is_null($username)) {

            $userRepository = $this->getDoctrine()->getManager()->getRepository('LaAdminBundle:User');
            $user = $userRepository->findOneByUsername($username);

            if (!is_null($user)) {
                return new RedirectResponse($this->container->get('router')->generate('la_admin_settings_user_edit',
                    array(
                        'admin_user' => $username)
                ));
            } else {
                $this->alert('danger', 'Le username spécifié n\'est pas dans la base');

                return $this->render('LaAdminBundle:Settings:user_search.html.twig', array(
                    'form' => $form->createView(),
                    'current_menu' => $this->currentMenu,
                    'current_sub_menu' => 'la_admin.nav.settings.users.edit'
                ));
            }
        } else {
            return $this->render('LaAdminBundle:Settings:user_search.html.twig', array(
                'form' => $form->createView(),
                'current_menu' => $this->currentMenu,
                'current_sub_menu' => 'la_admin.nav.settings.users.edit'
            ));
        }


    }


    /**
     * search a user
     *
     **/
    public function editUserAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_USER')) {
            throw new AccessDeniedException();
        }

        if ($request->query->has('admin_user')) {
            $username = $request->query->get('admin_user');
        }

        if (!is_null($username)) {
            // username found
            $em = $this->getDoctrine()->getManager();
            $userRepository = $em->getRepository('LaAdminBundle:User');
            $groupRepository = $em->getRepository('LaAdminBundle:Group');
            $user = $userRepository->findOneByUsername($username);
            $groups = $groupRepository->findAll();

            if (!is_null($user)) {
                $this->history('admin_user', $username);
                $oldPassword = $user->getPassword();
                $form = $this->container->get('form.factory')->createBuilder('admin_user', $user)->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if (null !== $user->getPassword()) {

                        $factory = $this->get('security.encoder_factory');
                        $encoder = $factory->getEncoder($user);
                        if ($user->getSalt() === '') {
                            $salt = md5(uniqid(null, true));
                            $user->setSalt($salt);
                        }
                        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                        $user->setPassword($password);
                    } else {

                        $user->setPassword($oldPassword);
                    }
                    $em->persist($user);
                    $em->flush();

                    $this->alert('success', 'Le user a été mis à jour');
                }

                return $this->render('LaAdminBundle:Settings:user.html.twig', array(
                    'form' => $form->createView(),
                    'user' => $user,
                    'current_menu' => $this->currentMenu,
                    'current_sub_menu' => 'la_admin.nav.settings.users.edit'
                ));
            } else {
                $this->alert('danger', 'Le username spécifié n\'est pas dans la base');
                return new RedirectResponse($this->container->get('router')->generate('la_admin_settings_users',
                    array(
                        'admin_user' => $username)
                ));
            }
        } else {
            $this->alert('danger', 'Le username spécifié n\'est pas dans la base');
            return new RedirectResponse($this->container->get('router')->generate('la_admin_settings_users',
                array(
                    'admin_user' => $username)
            ));
        }

    }


    /**
     * search a user
     *
     **/
    public function createUserAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_USER')) {
            throw new AccessDeniedException();
        }

        $user = new User();
        $form = $this->container->get('form.factory')->createBuilder('admin_user_register', $user)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $user = $form->getData();

            // user already exists ?
            $em = $this->getDoctrine()->getManager();
            $userRepository = $em->getRepository('LaAdminBundle:User');
            $existingUser = $userRepository->findOneByUsername($user->getUsername());
            $existingEMail = $userRepository->findOneByEmail($user->getEmail());
            if (!is_null($existingUser)) {
                $this->alert('danger', 'Le user est déjà dans la base');
            } elseif (!is_null($existingEMail)) {
                $this->alert('danger', 'Cet email est déjà dans la base');
            } else {

                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($password);

                $em->persist($user);
                $em->flush();

                $this->history('admin_user', $user->getUsername());
                $this->alert('success', 'Le user a été créé');

                return new RedirectResponse($this->container->get('router')->generate('la_admin_settings_user_edit',
                    array(
                        'admin_user' => $user->getUsername())
                ));
            }
        }

        return $this->render('LaAdminBundle:Settings:createuser.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_admin.nav.settings.users.create'
        ));

    }
}