<?php

namespace La\AdminBundle\Controller;

use La\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\HttpFoundation\Request;
use La\AdminBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends AdminController
{

    protected $currentMenu = 'la_admin.nav.settings.default';

    public function indexAction()
    {
        // ROLE_LA_ADMIN may be ommited
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }
        return $this->render('LaAdminBundle:Default:index.html.twig', array(
            'current_menu' => 'la_admin.nav.home.default',
            'current_sub_menu' => 'la_admin.nav.home.welcome'
        ));
    }

    public function sidebarAction($current_menu, $current_sub_menu)
    {

        $securedNav = $this->getSecuredNav($current_menu, $current_sub_menu);

        return $this->render('LaAdminBundle:Default:sidebar.html.twig', array(
            'links' => $securedNav,
            'current_menu' => $current_menu,
            'current_sub_menu' => $current_sub_menu
        ));
    }

    public function homeAction($current_menu, $current_sub_menu)
    {

        $securedNav = $this->getSecuredNav($current_menu, $current_sub_menu);

        return $this->render('LaAdminBundle:Default:home.html.twig', array(
            'links' => $securedNav,
            'current_menu' => $current_menu,
            'current_sub_menu' => $current_sub_menu
        ));
    }

    protected function getSecuredNav($current_menu, $current_sub_menu)
    {
        $nav = $this->get('la_admin.model')->getNav();
        $securedNav = [];

        foreach ($nav as $index => $item) {
            $granted = false;
            foreach ($item['links'] as $i) {
                if ($this->get('security.context')->isGranted($i['role'])) {
                    $granted = true;
                }
            }
            if ($granted) {
                $securedNav[$index] = $item;
                $securedNav[$index]['links'] = [];
                foreach ($item['links'] as $i) {
                    if ($this->get('security.context')->isGranted($i['role'])) {
                        $securedNav[$index]['links'][] = $i;
                    }
                }
            }
        }

        return $securedNav;
    }


    public function titleAction()
    {
        $title = $this->container->getParameter('la_admin.title');
        return $this->render('LaAdminBundle:Default:title.html.twig', array(
            'title' => $title
        ));
    }


}