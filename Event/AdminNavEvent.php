<?php

namespace La\AdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class AdminNavEvent extends Event
{
    protected $links = [];
    protected $name;

    public function addLinks($links)
    {
        $this->links[$links['name']] = $links;
    }

    public function getLinks()
    {
        return $this->links;
    }


}