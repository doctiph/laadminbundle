<?php

namespace La\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('la_admin');

        $rootNode
            ->children()
                ->scalarNode('title')
                    ->cannotBeEmpty()
                    ->defaultValue('Admin')
                ->end()
                ->arrayNode('roles')
                    ->useAttributeAsKey('roles')
                    ->prototype('array')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('name')
                            ->isRequired()
                        ->end()
                        ->scalarNode('parent')
                            ->defaultValue('ROLE_LA_ADMIN_SUPER_ADMIN')
                        ->end()
                        ->scalarNode('group')
                        ->end()
                    ->end()
                ->end()

            ->end();
        return $treeBuilder;
    }
}



