<?php

namespace La\AdminBundle\Model;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use La\AdminBundle\Event\AdminNavEvent;
use La\AdminBundle\Entity\Group;

class Admin

{

    protected $eventDispatcher;
    protected $nav = null;

    public function __construct(EventDispatcherInterface $eventDispatcher, $em, $rolesConf)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->em = $em;
        $this->rolesConf = $rolesConf;
    }


    public function getNav()
    {
        if (is_null($this->nav)) {
            $event = new AdminNavEvent();
            $this->eventDispatcher->dispatch('admin.nav.links', $event);
            $this->nav = $event->getLinks();
        }
        return $this->nav;
    }

    public function getRoles()
    {
        $event = new AdminNavEvent();
        $this->eventDispatcher->dispatch('admin.nav.links', $event);
    }

    public function getRoleList()
    {
        $roles = $this->em->getRepository('LaAdminBundle:Group')->findAll();
        if (count($roles) !== count($this->rolesConf)) {
            // Set role
            foreach ($this->rolesConf as $role => $roleData) {
                $existingRole = $this->em->getRepository('LaAdminBundle:Group')->findByRole($role);
                if (!count($existingRole)) {
                    $newRole = new Group();
                    $newRole->setName($roleData['name']);
                    $newRole->setRole($role);
                    $newRole->setGroupname($roleData['group']);
                    $this->em->persist($newRole);
                    $this->em->flush();
                }
            }
            // set Parents
            foreach ($this->rolesConf as $role => $roleData) {
                if (empty($roleData['parent'])) {
                    continue;
                }
                $existingRole = $this->em->getRepository('LaAdminBundle:Group')->findOneByRole($role);
                if (!count($existingRole)) {
                    continue;
                }
                $roleParent = $this->em->getRepository('LaAdminBundle:Group')->findOneByRole($roleData['parent']);
                if (!count($roleParent)) {
                    continue;
                }
                $existingRole->setParent($roleParent);
                $existingRole->setGroupname($roleData['group']);
                $this->em->persist($roleParent);
                $this->em->flush();
            }
        }
        return $roles;
    }

} 