<?php


namespace La\AdminBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', null, array('read_only' => true,'disabled' => true));
        $builder->add('email', 'email', array('required' => true));
        $builder->add('password', 'password', array('required' => false));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'La\AdminBundle\Entity\User',
        ));
    }

    public function getName()
    {
        return 'admin_account';
    }
}