<?php


namespace La\AdminBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{

    public function __construct($subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', null, array('read_only' => true));
        $builder->add('email', 'email', array('required' => true));
        $builder->add('password', 'password', array('required' => false));
        $builder->add('isActive', 'choice', array(
            'expanded' => false,
            'choices' => array(true => 'Oui', false => 'Non'),
        ));
        $builder->addEventSubscriber($this->subscriber);

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'La\AdminBundle\Entity\User',
        ));
    }

    public function getName()
    {
        return 'admin_user';
    }
}