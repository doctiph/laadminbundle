<?php


namespace La\AdminBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserRegisterType extends UserType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', null, array('read_only' => false));
        $builder->add('email', 'email', array('required' => true));
        $builder->add('password', 'password', array('required' => false));
        $builder->add('isActive', 'choice', array(
            'expanded' => false,
            'choices' => array(true => 'Oui', false => 'Non'),
        ));
        $builder->addEventSubscriber($this->subscriber);
    }

    public function getName()
    {
        return 'admin_user_register';
    }

}