<?php
namespace La\AdminBundle\Form\EventListener;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Ajoute des champs Role dynamiquement aux formulaires User
 *
 */
class AddRoleFieldsSubscriber implements EventSubscriberInterface
{
    protected $em;
    protected $securityContext;
    protected $rolesConf;

    public function  __construct($em, $securityContext, $manager)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::POST_SUBMIT => 'onPostSubmit',
        );
    }

    /**
     * @param $event
     */
    public function onPostSubmit($event)
    {
        $form = $event->getForm();
        $user = $event->getData();
        $availableRoles = $this->manager->getRoleList();
        foreach ($availableRoles as $group) {
            if ($group->getRole() === 'ROLE_LA_ADMIN') {
                $roleUserAdmin = $group;
                continue;
            }

            if (!$this->securityContext->isGranted('ROLE_LA_ADMIN_SUPER_ADMIN') && !$this->securityContext->isGranted($group->getRole())) {
                continue;
            }

            $formCheckbox = $form->get('role_' . $group->getId());

            switch ($formCheckbox->getData()) {
                case 'yes':
                    if (!$user->getGroups()->contains($group)) {
                        $user->getGroups()->add($group);
                    }
                    break;
                case 'no':
                    if ($user->getGroups()->contains($group)) {
                        $user->getGroups()->removeElement($group);
                    }
                    break;
            }

        }
        if (!$user->getGroups()->contains($roleUserAdmin)) {
            $user->getGroups()->add($roleUserAdmin);
        }
    }

    public function preSetData(FormEvent $event)
    {
        $user = $event->getData();

        $userRoles = [];

        if (!is_null($user)) {
            $roles = $user->getRoles();
            foreach ($roles as $role) {
                $userRoles[] = $role->getId();
            }
        }

        $availableRoles = $this->manager->getRoleList();

        $form = $event->getForm();

        foreach ($availableRoles as $group) {
            if ($group->getRole() === 'ROLE_LA_ADMIN') {
                $roleUserAdmin = $group;
                continue;
            }
            if (!$this->securityContext->isGranted('ROLE_LA_ADMIN_SUPER_ADMIN') && !$this->securityContext->isGranted($group->getRole())) {
                continue;
            }
            if (in_array($group->getId(), $userRoles)) {
                $this->add($user, $form, $group, true);
            } else {
                $this->add($user, $form, $group);
            }
        }

    }

    protected function add($user, $form, $group, $checked = false)
    {
        if (!$this->securityContext->isGranted('ROLE_LA_ADMIN_SUPER_ADMIN') && !$this->securityContext->isGranted($group->getRole())) {
            return;
        }
        $readOnly = false;
        if ($this->securityContext->getToken()->getUser() === $user) {
            $readOnly = true;
            $checked = true;
        }
        $label = sprintf('[%s] %s', $group->getGroupname(), $group->getName());
        $options = array(
            'label' => $label,
            'mapped' => false,
            'expanded' => false,
            'required' => true,
            'read_only' => $readOnly,
            'disabled' => $readOnly,
            'choices' => array('yes' => 'Oui', 'no' => 'Non'),
            'preferred_choices' => array('no'),
            'constraints' => array(
                new NotNull(array(
                    'message' => 'Choisissez une option'
                )),
            )
        );

        if ($checked) {
            $options['data'] = 'yes';
        }

        $form->add('role_' . $group->getId(), 'choice', $options);
    }
}