INSTALLATION

composer.json

    "repositories": [
        ....
        {
            "type": "git",
            "url": "git@git-pool01.in.ladtech.fr:LaAdminBundle"
        }
    ],


 "require": {
        ....
        "la/adminbundle": "dev-master"
    },


app/AppKernel.php
        ....
        new La\AdminBundle\LaAdminBundle(),


#Database update

php app/console doctrine:schema:update --dump-sql
INSERT INTO `laadmin__groups` (`id`, `parent_id`, `name`, `role`) VALUES ('1', NULL, 'Admin de l''admin', 'ROLE_LA_ADMIN_SUPER_ADMIN');
INSERT INTO `laadmin__users` (`id`, `username`, `salt`, `password`, `email`, `is_active`) VALUES (1, 'admin', '', '387cbad88dd61cd2864f922bbe26db54e759b9c1', 'admin@admin', 1); #Yvette2017;
INSERT INTO `laadmin__users_group` (`user_id`, `group_id`) VALUES ('1', '1');


app/config/security.yml

security:
    encoders:
        ....
        La\AdminBundle\Entity\User:
            algorithm:        sha1
            encode_as_base64: false
            iterations:       1
    providers:
        ....
        la_admin:
            entity: { class: LaAdminBundle:User }
    firewalls:
        ....
        la_admin_area:
            pattern:    ^/admin
            http_basic:
                realm: "Secured La Admin Area"
                provider: la_admin

    access_control:
        ....
        - { path: ^/admin, roles: ROLE_LA_ADMIN }


app/config/routing.yml

la_user_admin:
    resource: "@LaUserBundle/Resources/config/routing/admin.yml"
    prefix:   /admin

la_newsletter_admin:
    resource: "@LaNewsletterBundle/Resources/config/routing/admin.yml"
    prefix:   /admin

la_admin:
    resource: "@LaAdminBundle/Resources/config/routing.yml"
    prefix:   /admin

app/config/config.yml

imports:
    ....
    - { resource: @LaAdminBundle/Resources/config/config.yml }

app/config/la_config.yml
la_admin:
    title: 'Administraion des Users de sitename.tld'

php app/console assets:install --symlink --relative

Aller dans l'édition de son compte et le mettre à jour
